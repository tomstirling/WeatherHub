package weatherHub

/**
 * Format file data to be more suitable for cmd output.
 */
class Formatter {

    /**
     * Formats a [month] numerical value, converting the input to a text representation.
     **/
    fun formatMonth(month: String?): String {
        return when(month) {
            "1" -> "January"
            "2" -> "February"
            "3" -> "March"
            "4" -> "April"
            "5" -> "May"
            "6" -> "June"
            "7" -> "July"
            "8" -> "August"
            "9" -> "September"
            "10" -> "October"
            "11" -> "November"
            "12" -> "December"
            else -> throw error("\n- ERROR: FILE CONTAINS INVALID MONTH")
        }
    }
}