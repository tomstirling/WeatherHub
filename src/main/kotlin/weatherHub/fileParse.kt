package weatherHub

import java.io.File
import kotlin.system.exitProcess


/**
 * Parse an input file and extract data*.
 *
 * Separately parse the first 4 lines of a file (headers) & the remaining lines (records) returning
 * values of the corresponding data class type.
 *
 * @property fileName the name of the input file.
 */
class FileParse(fileName: String) {
    private val file = File(fileName)

    /**
     * Parses the headers of the inputted file, splitting the input string by spaces and removing
     * any empty element.
     *
     * @return HeaderData constructor values: Station, Latitude, Longitude, Elevation as the
     * correct type.
     */
    fun parseHeaders(): HeaderData {
        val fileString = file.bufferedReader().readLines()

        if (fileString.count() < 4) { println("\n- ERROR: FILE IS MISSING HEADERS"); exitProcess(0) }

        val slicedString = fileString.slice(0..3)
        val splitString = slicedString.toString().replace("[\\[\\]',]".toRegex(), "")
                .split(" ")
                .toMutableList()

        splitString.removeAll(listOf("", null))

        return HeaderData(splitString[0],splitString[5].toFloat(), splitString[7].toFloat(), splitString[8].toInt())
    }

    /**
     * Parses the records of the inputted file, splitting the input string by spaces and removing
     * any empty element for each record. A final map is produced, mapping the correct record value
     * to the corresponding key.
     *
     * simple validation is also handled here, quality checking the records while parsing.
     *
     * @return RecordData Array, containing a map for each individual record.
     */
    fun parseRecords(): RecordData {
        val totalSize = file.bufferedReader().lines().count().toString()
        val fileString = file.bufferedReader().readLines().slice(4 until totalSize.toInt()).toString()
        val parsedRecord = fileString.split(" ").toString()
                .replace("[\\[\\]',]".toRegex(), "")
                .split(" ")
                .toMutableList()

        repeat(parsedRecord.size) { parsedRecord.removeAll(listOf("", null)) }

        val finalParsedRecord = parsedRecord.chunked(7)
        val array: ArrayList<HashMap<String, String>> = arrayListOf()

        finalParsedRecord.forEach { record ->
            val rec = hashMapOf("yyyy" to record[0], "mm" to record[1], "tmax" to record[2],
                    "tmin" to record[3], "af" to record[4], "rain" to record[5], "sun" to record[6])

            when {
                record[5].toFloat() < 0.0 -> {
                    println("\n- ERROR: FILE CONTAINS INVALID RAINFALL VALUE (negative value)"); exitProcess(0)
                }
                record[1].toInt() !in 1..12 -> {
                    println("\n- ERROR: FILE CONTAINS INVALID MONTH (not in range 1 - 12)"); exitProcess(0)
                }
                record[0].toInt() < 1930 -> {
                    println("\n- ERROR: FILE CONTAINS INVALID YEAR (earlier than 1930)"); exitProcess(0)
                }
            }
            array.add(rec)
        }
        return RecordData(array)
    }
}
