package weatherHub

/**
 * HeaderData data class, containing required data types for file headers.
 */
data class HeaderData(
        val station: String,
        val latitude: Float,
        val longitude: Float,
        val elevation: Int
)

/**
 * RecordData data class, containing required data type (collection) for all records.
 */
data class RecordData(
        val Records: ArrayList<HashMap<String, String>>
)

/**
 * RecordRainData data class, containing required data types for record-rainfall.
 */
data class RecordRainData(
        val month: String?,
        val year: Int,
        val rain: Float
)

/**
 * YearRangeData data class, containing required data types for years.
 */
data class YearRangeData(
        val earliest: Int,
        val latest: Int
)