package weatherHub

import java.io.File
import java.util.HashMap
import kotlin.system.exitProcess


/**
 * Extract & process record data provided in the input file*.
 *
 * @property fileName the name of the input file.
 */
class Record(fileName: String) {
    private val file = File(fileName)
    private val data = FileParse(file.toString()).parseRecords().Records
    private val groupedYearMap = data.groupBy { it["yyyy"] }.toMap()

    /**
     * Gets total number of records in the file by checking the size.
     *
     * @return the size of the record map.
     */
    fun getNumberOfRecords(): Int {
        if (data.size == 0) {
            println("\n- ERROR: FILE HAS NO RECORDS TO DISPLAY"); exitProcess(0) }

        return data.size
    }

    /**
     * Gets total number of records in the file by checking the size.
     *
     * @return two integers representing the earlier & most recent year of all records in the data set.
     */
    fun getYearsSpanned(): YearRangeData {
        if (data.size == 0) {
            println("\n- ERROR: FILE HAS NO RECORDS TO DISPLAY"); exitProcess(0) }

        val early = data.minBy { it["yyyy"]!!.toInt() }?.get("yyyy")!!.toInt()
        val late = data.maxBy { it["yyyy"]!!.toInt() }?.get("yyyy")!!.toInt()

        return YearRangeData(early, late)
    }

    /**
     * Gets the total rainfall value for each year.
     *
     * Groups the records by years, then proceeds to loop and add values if the key is equal, else it resets
     * the rainfall counter for a new year.
     *
     * @return an array containing all total rain values for each year.
     */
    private fun sortTotalRainByYear(): ArrayList<Pair<Int?, Float>> {
        var totalRain = 0F
        val totalRainArr: ArrayList<Pair<Int?, Float>> = ArrayList()

        groupedYearMap.forEach { (key, value) ->
            value.forEach { index ->
                val rain = index["rain"]
                val year = index["yyyy"]
                if (key == year) totalRain += rain!!.toFloat() }
            totalRainArr.add(Pair(key?.toInt(), totalRain))
            totalRain = 0F }

        return totalRainArr
    }

    /**
     * Gets the wettest year, utilizing the array created in the [sortTotalRainByYear] function.
     *
     * @return the year & total rainfall value of the wettest year.
     */
    fun getWettestYear(): RecordRainData {
        val mostRain = sortTotalRainByYear().maxBy { it.second }

        return RecordRainData(null, mostRain!!.first!!, mostRain.second)
    }

    /**
     * Gets the driest year, utilizing the array created in the [sortTotalRainByYear] function.
     *
     * @return the year & total rainfall value of the driest year.
     */
    fun getDriestYear(): RecordRainData {
        val leastRain = sortTotalRainByYear().minBy { it.second }

        return RecordRainData(null, leastRain!!.first!!, leastRain.second)
    }

    /**
     * Gets the wettest month from all records.
     *
     * @return the month, year & rainfall value of the wettest month.
     */
    fun getWettestMonth(): RecordRainData {
        val max = data.maxBy { it["rain"]!!.toFloat() }
        val mm = max?.get("mm").toString()
        val yyyy = max?.get("yyyy")!!.toInt()
        val rain = max["rain"]!!.toFloat()

        return RecordRainData(mm, yyyy, rain)
    }

    /**
     * Gets the driest month from all records.
     *
     * @return the month, year & rainfall value of the driest month.
     */
    fun getDriestMonth(): RecordRainData {
        val min = data.minBy { it["rain"]!!.toFloat() }
        val mm = min?.get("mm").toString()
        val yyyy = min?.get("yyyy")!!.toInt()
        val rain = min["rain"]!!.toFloat()

        return RecordRainData(mm, yyyy, rain)
    }

    /**
     * Display rainfall bar-chart, showing a distribution of rainfall values for each month, for a given [year].
     *
     * @return the output string of rainfall in each month for a given year.
     */
    fun showRainBarChart(year: Int): Map<String?, List<HashMap<String, String>>> {
        groupedYearMap.forEach { (key, value) ->
            val maxValue = value.maxBy { it["rain"]!!.toFloat() }?.get("rain")?.toFloat()
            if (key == year.toString()) {
                value.forEach {
                    val percentVal = (it["rain"]!!.toFloat().div(maxValue!!).times(100)) / 2
                    val output = "#".repeat((percentVal.toInt()))
                    val month = Formatter().formatMonth(it["mm"].toString())
                    System.out.printf("%9s (%5.1f): %s\n", month, it["rain"]?.toFloat(), output) } }
        }
        return groupedYearMap
    }
}
