package weatherHub

import java.io.File
import kotlin.system.exitProcess

/**
 * Calls & represents correct data in relation to the user specified [args].
 *
 * @return correctly formatted output for each inputted argument
 */
fun main (args: Array<String>) {
    if (args.isEmpty()) {
        println("\n- ERROR: NO FILE SELECTED (please input a valid [FILENAME][YEAR(optional)] as args)"); exitProcess(0) }

    val fileExists = File(args[0]).isFile
    val fn = File(args[0]).toString()

    if (!fileExists) { println("\n- ERROR: INVALID FILENAME (try 'data/filename.txt')"); exitProcess(0) }

    when (args.size) {
        2 -> {
            val record = Record(fn)
            val yearSpan = record.getYearsSpanned()

            if (args[1].toInt() < yearSpan.earliest || args[1].toInt() > yearSpan.latest) {
                System.out.printf("\n- ERROR: INVALID 2ND ARGUMENT [YEAR] (not in range %d - %d)\n",
                        yearSpan.earliest, yearSpan.latest); exitProcess(0) }
            else { record.showRainBarChart(args[1].toInt()); exitProcess(0) }
        }
        else -> {

            val data = FileParse(fn)
            val headers = data.parseHeaders()
            val fmt = Formatter()
            val record = Record(fn)

            System.out.printf("Station: %s\n", headers.station)
            System.out.printf("Latitude: %.3f\u00b0\n", headers.latitude)
            System.out.printf("Longitude: %.3f\u00b0\n", headers.longitude)
            System.out.printf("Elevation: %d m\n", headers.elevation)

            val yearSpan = record.getYearsSpanned()
            val wetYear = record.getWettestYear()
            val dryYear = record.getDriestYear()
            val wetMonth = record.getWettestMonth()
            val dryMonth = record.getDriestMonth()

            System.out.printf("Number of records: %d\n", record.getNumberOfRecords())
            System.out.printf("Years spanned: %d to %d\n", yearSpan.earliest, yearSpan.latest)
            System.out.printf("Wettest year: %d (%.1f mm)\n", wetYear.year, wetYear.rain)
            System.out.printf("Driest year: %d (%.1f mm)\n", dryYear.year, dryYear.rain)
            System.out.printf("Wettest month: %s %d (%.1f mm)\n", fmt.formatMonth(wetMonth.month),
                    wetMonth.year, wetMonth.rain)
            System.out.printf("Driest month: %s %d (%.1f mm)\n", fmt.formatMonth(dryMonth.month),
                    dryMonth.year, dryMonth.rain)

            exitProcess(0)
        }
    }
}
