## Building The Application

You can build the application on the command line with

    ./gradlew build

## Running The Application

### With Gradle

You can run the application without command line arguments using

    ./gradlew run

Or with provided data:

    ./gradlew run --args data/bradford.txt

To specify a data filename and a year as command line arguments, you
need to enclose both in quotes, like this:

    ./gradlew run --args "data/bradford.txt 2018"